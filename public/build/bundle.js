
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.head.appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    function assign(tar, src) {
        // @ts-ignore
        for (const k in src)
            tar[k] = src[k];
        return tar;
    }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function create_slot(definition, ctx, $$scope, fn) {
        if (definition) {
            const slot_ctx = get_slot_context(definition, ctx, $$scope, fn);
            return definition[0](slot_ctx);
        }
    }
    function get_slot_context(definition, ctx, $$scope, fn) {
        return definition[1] && fn
            ? assign($$scope.ctx.slice(), definition[1](fn(ctx)))
            : $$scope.ctx;
    }
    function get_slot_changes(definition, $$scope, dirty, fn) {
        if (definition[2] && fn) {
            const lets = definition[2](fn(dirty));
            if (typeof $$scope.dirty === 'object') {
                const merged = [];
                const len = Math.max($$scope.dirty.length, lets.length);
                for (let i = 0; i < len; i += 1) {
                    merged[i] = $$scope.dirty[i] | lets[i];
                }
                return merged;
            }
            return $$scope.dirty | lets;
        }
        return $$scope.dirty;
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function to_number(value) {
        return value === '' ? undefined : +value;
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function set_input_value(input, value) {
        if (value != null || input.value) {
            input.value = value;
        }
    }
    function select_option(select, value) {
        for (let i = 0; i < select.options.length; i += 1) {
            const option = select.options[i];
            if (option.__value === value) {
                option.selected = true;
                return;
            }
        }
    }
    function select_value(select) {
        const selected_option = select.querySelector(':checked') || select.options[0];
        return selected_option && selected_option.__value;
    }
    function toggle_class(element, name, toggle) {
        element.classList[toggle ? 'add' : 'remove'](name);
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error(`Function called outside component initialization`);
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    const seen_callbacks = new Set();
    function flush() {
        do {
            // first, call beforeUpdate functions
            // and update components
            while (dirty_components.length) {
                const component = dirty_components.shift();
                set_current_component(component);
                update(component.$$);
            }
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    let outros;
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function transition_out(block, local, detach, callback) {
        if (block && block.o) {
            if (outroing.has(block))
                return;
            outroing.add(block);
            outros.c.push(() => {
                outroing.delete(block);
                if (callback) {
                    if (detach)
                        block.d(1);
                    callback();
                }
            });
            block.o(local);
        }
    }
    function create_component(block) {
        block && block.c();
    }
    function mount_component(component, target, anchor) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        // onMount happens before the initial afterUpdate
        add_render_callback(() => {
            const new_on_destroy = on_mount.map(run).filter(is_function);
            if (on_destroy) {
                on_destroy.push(...new_on_destroy);
            }
            else {
                // Edge case - component was destroyed immediately,
                // most likely as a result of a binding initialising
                run_all(new_on_destroy);
            }
            component.$$.on_mount = [];
        });
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const prop_values = options.props || {};
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : []),
            // everything else
            callbacks: blank_object(),
            dirty
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, prop_values, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if ($$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(children(options.target));
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor);
            flush();
        }
        set_current_component(parent_component);
    }
    class SvelteComponent {
        $destroy() {
            destroy_component(this, 1);
            this.$destroy = noop;
        }
        $on(type, callback) {
            const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
            callbacks.push(callback);
            return () => {
                const index = callbacks.indexOf(callback);
                if (index !== -1)
                    callbacks.splice(index, 1);
            };
        }
        $set() {
            // overridden by instance, if it has props
        }
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.18.1' }, detail)));
    }
    function append_dev(target, node) {
        dispatch_dev("SvelteDOMInsert", { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev("SvelteDOMInsert", { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev("SvelteDOMRemove", { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ["capture"] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev("SvelteDOMAddEventListener", { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev("SvelteDOMRemoveEventListener", { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev("SvelteDOMRemoveAttribute", { node, attribute });
        else
            dispatch_dev("SvelteDOMSetAttribute", { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.data === data)
            return;
        dispatch_dev("SvelteDOMSetData", { node: text, data });
        text.data = data;
    }
    class SvelteComponentDev extends SvelteComponent {
        constructor(options) {
            if (!options || (!options.target && !options.$$inline)) {
                throw new Error(`'target' is a required option`);
            }
            super();
        }
        $destroy() {
            super.$destroy();
            this.$destroy = () => {
                console.warn(`Component was already destroyed`); // eslint-disable-line no-console
            };
        }
    }

    /* src\components\Card.svelte generated by Svelte v3.18.1 */

    const file = "src\\components\\Card.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[7] = list[i];
    	return child_ctx;
    }

    // (16:16) {#each links as link}
    function create_each_block(ctx) {
    	let a;
    	let t_value = /*link*/ ctx[7].text + "";
    	let t;
    	let a_href_value;

    	const block = {
    		c: function create() {
    			a = element("a");
    			t = text(t_value);
    			attr_dev(a, "href", a_href_value = /*link*/ ctx[7].href);
    			attr_dev(a, "class", "blue-text text-darken-2");
    			add_location(a, file, 16, 20, 759);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, a, anchor);
    			append_dev(a, t);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*links*/ 2 && t_value !== (t_value = /*link*/ ctx[7].text + "")) set_data_dev(t, t_value);

    			if (dirty & /*links*/ 2 && a_href_value !== (a_href_value = /*link*/ ctx[7].href)) {
    				attr_dev(a, "href", a_href_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(a);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(16:16) {#each links as link}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let div4;
    	let div3;
    	let div2;
    	let div0;
    	let span;
    	let t0;
    	let t1;
    	let p;
    	let div0_class_value;
    	let t2;
    	let div1;
    	let div2_class_value;
    	let current;
    	const default_slot_template = /*$$slots*/ ctx[6].default;
    	const default_slot = create_slot(default_slot_template, ctx, /*$$scope*/ ctx[5], null);
    	let each_value = /*links*/ ctx[1];
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div4 = element("div");
    			div3 = element("div");
    			div2 = element("div");
    			div0 = element("div");
    			span = element("span");
    			t0 = text(/*title*/ ctx[0]);
    			t1 = space();
    			p = element("p");
    			if (default_slot) default_slot.c();
    			t2 = space();
    			div1 = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			attr_dev(span, "class", "card-title");
    			add_location(span, file, 11, 16, 567);
    			add_location(p, file, 12, 16, 624);
    			attr_dev(div0, "class", div0_class_value = "card-content " + /*textColor*/ ctx[3]);
    			add_location(div0, file, 10, 12, 511);
    			attr_dev(div1, "class", "card-action");
    			add_location(div1, file, 14, 12, 673);
    			attr_dev(div2, "class", div2_class_value = "card " + /*color*/ ctx[2]);
    			add_location(div2, file, 9, 8, 471);
    			attr_dev(div3, "class", "col s12");
    			add_location(div3, file, 8, 4, 440);
    			attr_dev(div4, "class", "row");
    			add_location(div4, file, 7, 0, 417);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div4, anchor);
    			append_dev(div4, div3);
    			append_dev(div3, div2);
    			append_dev(div2, div0);
    			append_dev(div0, span);
    			append_dev(span, t0);
    			append_dev(div0, t1);
    			append_dev(div0, p);

    			if (default_slot) {
    				default_slot.m(p, null);
    			}

    			append_dev(div2, t2);
    			append_dev(div2, div1);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div1, null);
    			}

    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			if (!current || dirty & /*title*/ 1) set_data_dev(t0, /*title*/ ctx[0]);

    			if (default_slot && default_slot.p && dirty & /*$$scope*/ 32) {
    				default_slot.p(get_slot_context(default_slot_template, ctx, /*$$scope*/ ctx[5], null), get_slot_changes(default_slot_template, /*$$scope*/ ctx[5], dirty, null));
    			}

    			if (!current || dirty & /*textColor*/ 8 && div0_class_value !== (div0_class_value = "card-content " + /*textColor*/ ctx[3])) {
    				attr_dev(div0, "class", div0_class_value);
    			}

    			if (dirty & /*links*/ 2) {
    				each_value = /*links*/ ctx[1];
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div1, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (!current || dirty & /*color*/ 4 && div2_class_value !== (div2_class_value = "card " + /*color*/ ctx[2])) {
    				attr_dev(div2, "class", div2_class_value);
    			}
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(default_slot, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(default_slot, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div4);
    			if (default_slot) default_slot.d(detaching);
    			destroy_each(each_blocks, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { title = "Title" } = $$props;
    	let { content = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos amet quam blanditiis soluta ratione cupiditate tempore facere perspiciatis quidem recusandae." } = $$props;
    	let { links = [{ href: "#", text: "First link" }, { href: "#", text: "Second link" }] } = $$props;
    	let { color = "white" } = $$props;
    	let { textColor = "grey-text text-darken-3" } = $$props;
    	const writable_props = ["title", "content", "links", "color", "textColor"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<Card> was created with unknown prop '${key}'`);
    	});

    	let { $$slots = {}, $$scope } = $$props;

    	$$self.$set = $$props => {
    		if ("title" in $$props) $$invalidate(0, title = $$props.title);
    		if ("content" in $$props) $$invalidate(4, content = $$props.content);
    		if ("links" in $$props) $$invalidate(1, links = $$props.links);
    		if ("color" in $$props) $$invalidate(2, color = $$props.color);
    		if ("textColor" in $$props) $$invalidate(3, textColor = $$props.textColor);
    		if ("$$scope" in $$props) $$invalidate(5, $$scope = $$props.$$scope);
    	};

    	$$self.$capture_state = () => {
    		return { title, content, links, color, textColor };
    	};

    	$$self.$inject_state = $$props => {
    		if ("title" in $$props) $$invalidate(0, title = $$props.title);
    		if ("content" in $$props) $$invalidate(4, content = $$props.content);
    		if ("links" in $$props) $$invalidate(1, links = $$props.links);
    		if ("color" in $$props) $$invalidate(2, color = $$props.color);
    		if ("textColor" in $$props) $$invalidate(3, textColor = $$props.textColor);
    	};

    	return [title, links, color, textColor, content, $$scope, $$slots];
    }

    class Card extends SvelteComponentDev {
    	constructor(options) {
    		super(options);

    		init(this, options, instance, create_fragment, safe_not_equal, {
    			title: 0,
    			content: 4,
    			links: 1,
    			color: 2,
    			textColor: 3
    		});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Card",
    			options,
    			id: create_fragment.name
    		});
    	}

    	get title() {
    		throw new Error("<Card>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set title(value) {
    		throw new Error("<Card>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get content() {
    		throw new Error("<Card>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set content(value) {
    		throw new Error("<Card>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get links() {
    		throw new Error("<Card>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set links(value) {
    		throw new Error("<Card>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get color() {
    		throw new Error("<Card>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set color(value) {
    		throw new Error("<Card>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get textColor() {
    		throw new Error("<Card>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set textColor(value) {
    		throw new Error("<Card>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    /* src\components\charts\LineChart.svelte generated by Svelte v3.18.1 */
    const file$1 = "src\\components\\charts\\LineChart.svelte";

    function create_fragment$1(ctx) {
    	let div;

    	const block = {
    		c: function create() {
    			div = element("div");
    			attr_dev(div, "class", "ct-chart ct-minor-seventh");
    			attr_dev(div, "id", /*chartId*/ ctx[0]);
    			add_location(div, file$1, 13, 0, 251);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*chartId*/ 1) {
    				attr_dev(div, "id", /*chartId*/ ctx[0]);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$1.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$1($$self, $$props, $$invalidate) {
    	let { chartId } = $$props;
    	let { series = [[1, 2, 3, 4, 5]] } = $$props;
    	let { labels = ["A", "B", "C", "D", "E"] } = $$props;

    	onMount(() => {
    		new Chartist.Line(`#${chartId}`, { labels, series });
    	});

    	const writable_props = ["chartId", "series", "labels"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<LineChart> was created with unknown prop '${key}'`);
    	});

    	$$self.$set = $$props => {
    		if ("chartId" in $$props) $$invalidate(0, chartId = $$props.chartId);
    		if ("series" in $$props) $$invalidate(1, series = $$props.series);
    		if ("labels" in $$props) $$invalidate(2, labels = $$props.labels);
    	};

    	$$self.$capture_state = () => {
    		return { chartId, series, labels };
    	};

    	$$self.$inject_state = $$props => {
    		if ("chartId" in $$props) $$invalidate(0, chartId = $$props.chartId);
    		if ("series" in $$props) $$invalidate(1, series = $$props.series);
    		if ("labels" in $$props) $$invalidate(2, labels = $$props.labels);
    	};

    	return [chartId, series, labels];
    }

    class LineChart extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$1, create_fragment$1, safe_not_equal, { chartId: 0, series: 1, labels: 2 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "LineChart",
    			options,
    			id: create_fragment$1.name
    		});

    		const { ctx } = this.$$;
    		const props = options.props || {};

    		if (/*chartId*/ ctx[0] === undefined && !("chartId" in props)) {
    			console.warn("<LineChart> was created without expected prop 'chartId'");
    		}
    	}

    	get chartId() {
    		throw new Error("<LineChart>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set chartId(value) {
    		throw new Error("<LineChart>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get series() {
    		throw new Error("<LineChart>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set series(value) {
    		throw new Error("<LineChart>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get labels() {
    		throw new Error("<LineChart>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set labels(value) {
    		throw new Error("<LineChart>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    const id = () => Date.now() + '_' + Math.random().toString(36).substr(2, 9);

    class Table {
        constructor(key) {
            this.key = key;
            let json = window.localStorage.getItem(key);

            if(!json) {
                json = "{}";
                window.localStorage.setItem(key, "{}");
            }
            this.data = JSON.parse(json);
        }

        save() {
            window.localStorage.setItem(this.key, JSON.stringify(this.data));
        }

        add(data) {
            let obj = data;
            obj.id = id();

            this.data[obj.id] = obj;
            this.save();

            return obj;
        }

        getById(id) {
            return this.data[id];
        }

        getByValue(key, value) {
            for(let objId in this.data) {
                let obj = this.data[objId];

                for(let field in obj) {
                    if(obj[field] === value) return obj;
                }
            }
        }

        getAll() {
            let ret = [];
            for(let objId in this.data) {
                ret.push(this.data[objId]);
            }
            return ret;
        }

        update(id, data) {
            this.data[id] = Object.assign(this.data[id], data);
            this.save();
            return this.data[id];
        }

        delete(id) {
            let deleted = delete this.data[id];
            this.save();

            return deleted;
        }

        getDateOfCreation(id) {
            return new Date(parseInt(id.split("_")[0]));
        }
    }

    // let jobs = new Table('jobs');
    // let paychecks = new Table('paychecks');
    // let tips = new Table('tips');
    // let expenses = new Table("expenses");

    //let job = jobs.add({name: "Dumsers Dairyland", tips: false, wage: 11});

    // let job = jobs.getById("1580848227171_orrumjh7q");
    // let job = jobs.getByValue("name", "Dumsers Dairyland");
    // console.log(job);
    // jobs.update(job.id, {wage: 100});
    // jobs.delete(job.id);
    // console.log(jobs.getAll());

    // paychecks.add({amount: 50 * job.wage, hours: 50, job_id: job.id}); 

    // let check = paychecks.add({amount: job.wage * 35, hours: 35, job_id: job.id});
    // let tip = tips.add({amount: 5, job_id: job.id});

    // console.log(tips.update(tip.id, {amount: 10}));

    // console.log("paycheck", check.amount);
    // console.log("tip", tip.amount, "on job", jobs.getById(tip.job_id).name)

    /* src\components\charts\PaycheckChart.svelte generated by Svelte v3.18.1 */

    function create_fragment$2(ctx) {
    	let current;

    	const chart = new LineChart({
    			props: {
    				series: /*data*/ ctx[0],
    				labels: /*labels*/ ctx[1],
    				chartId: "paychecks-chart"
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(chart.$$.fragment);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			mount_component(chart, target, anchor);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(chart.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(chart.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(chart, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$2.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$2($$self) {
    	const paychecks = new Table("paychecks");
    	const allPaychecks = paychecks.getAll();
    	console.log(allPaychecks);
    	let data = [[2, 2, 2, 2, 2]];
    	let labels = ["30.05", "14.06", "28.06", "12.07", "26.07"];

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("data" in $$props) $$invalidate(0, data = $$props.data);
    		if ("labels" in $$props) $$invalidate(1, labels = $$props.labels);
    	};

    	return [data, labels];
    }

    class PaycheckChart extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$2, create_fragment$2, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "PaycheckChart",
    			options,
    			id: create_fragment$2.name
    		});
    	}
    }

    /* src\components\pages\Home.svelte generated by Svelte v3.18.1 */
    const file$2 = "src\\components\\pages\\Home.svelte";

    // (7:4) <Card title="Test">
    function create_default_slot(ctx) {
    	let current;

    	const linechart = new PaycheckChart({
    			props: { chartId: "ashdiuh" },
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			create_component(linechart.$$.fragment);
    		},
    		m: function mount(target, anchor) {
    			mount_component(linechart, target, anchor);
    			current = true;
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(linechart.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(linechart.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(linechart, detaching);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_default_slot.name,
    		type: "slot",
    		source: "(7:4) <Card title=\\\"Test\\\">",
    		ctx
    	});

    	return block;
    }

    function create_fragment$3(ctx) {
    	let div;
    	let current;

    	const card = new Card({
    			props: {
    				title: "Test",
    				$$slots: { default: [create_default_slot] },
    				$$scope: { ctx }
    			},
    			$$inline: true
    		});

    	const block = {
    		c: function create() {
    			div = element("div");
    			create_component(card.$$.fragment);
    			attr_dev(div, "class", "wrapper svelte-1319hwk");
    			add_location(div, file$2, 5, 0, 116);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			mount_component(card, div, null);
    			current = true;
    		},
    		p: function update(ctx, [dirty]) {
    			const card_changes = {};

    			if (dirty & /*$$scope*/ 1) {
    				card_changes.$$scope = { dirty, ctx };
    			}

    			card.$set(card_changes);
    		},
    		i: function intro(local) {
    			if (current) return;
    			transition_in(card.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(card.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_component(card);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$3.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    class Home extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, null, create_fragment$3, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Home",
    			options,
    			id: create_fragment$3.name
    		});
    	}
    }

    /* src\components\pages\Edit.svelte generated by Svelte v3.18.1 */

    const file$3 = "src\\components\\pages\\Edit.svelte";

    function create_fragment$4(ctx) {
    	let h1;
    	let t1;
    	let h3;

    	const block = {
    		c: function create() {
    			h1 = element("h1");
    			h1.textContent = "Edit page";
    			t1 = space();
    			h3 = element("h3");
    			h3.textContent = "(CRUD style)";
    			add_location(h1, file$3, 0, 0, 0);
    			add_location(h3, file$3, 1, 0, 20);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, h1, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, h3, anchor);
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(h1);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(h3);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$4.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    class Edit extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, null, create_fragment$4, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Edit",
    			options,
    			id: create_fragment$4.name
    		});
    	}
    }

    const setPage = (Page, props, url) => {
        const wrapper = document.getElementsByClassName("wrapper")[0];    
    };

    const setPageByName = (pageName, props) => {
        currentPage = pageName;
        window.history.pushState({pageName, props}, "PeshkaWAT", "/");
        const Page = pages[pageName];
        const wrapper = document.getElementsByClassName("wrapper")[0];
        wrapper.innerHTML = ""; // Clear any page that is already loaded;
        new Page({
            target: wrapper,
            props
        });
    };


    var router = {
        setPage, setPageByName
    };

    let currentPage = "home";

    /* src\components\pages\Spend.svelte generated by Svelte v3.18.1 */
    const file$4 = "src\\components\\pages\\Spend.svelte";

    function create_fragment$5(ctx) {
    	let div3;
    	let h4;
    	let t1;
    	let div0;
    	let input0;
    	let input0_updating = false;
    	let t2;
    	let label0;
    	let t4;
    	let div1;
    	let input1;
    	let t5;
    	let label1;
    	let t7;
    	let div2;
    	let a;
    	let i;
    	let t9;
    	let dispose;

    	function input0_input_handler() {
    		input0_updating = true;
    		/*input0_input_handler*/ ctx[4].call(input0);
    	}

    	const block = {
    		c: function create() {
    			div3 = element("div");
    			h4 = element("h4");
    			h4.textContent = "Add Expense";
    			t1 = space();
    			div0 = element("div");
    			input0 = element("input");
    			t2 = space();
    			label0 = element("label");
    			label0.textContent = "Amount spent";
    			t4 = space();
    			div1 = element("div");
    			input1 = element("input");
    			t5 = space();
    			label1 = element("label");
    			label1.textContent = "Note";
    			t7 = space();
    			div2 = element("div");
    			a = element("a");
    			i = element("i");
    			i.textContent = "add";
    			t9 = text("Add");
    			add_location(h4, file$4, 32, 4, 845);
    			attr_dev(input0, "type", "number");
    			attr_dev(input0, "name", "amount");
    			add_location(input0, file$4, 34, 8, 914);
    			attr_dev(label0, "for", "amount");
    			add_location(label0, file$4, 35, 8, 979);
    			attr_dev(div0, "class", "col s12 input-field");
    			add_location(div0, file$4, 33, 4, 871);
    			attr_dev(input1, "type", "text");
    			attr_dev(input1, "name", "note");
    			add_location(input1, file$4, 38, 8, 1080);
    			attr_dev(label1, "for", "note");
    			add_location(label1, file$4, 39, 8, 1139);
    			attr_dev(div1, "class", "col s12 input-field");
    			add_location(div1, file$4, 37, 4, 1037);
    			attr_dev(i, "class", "material-icons left");
    			add_location(i, file$4, 42, 66, 1288);
    			attr_dev(a, "href", "#!");
    			attr_dev(a, "class", "btn green darken-1");
    			add_location(a, file$4, 42, 8, 1230);
    			attr_dev(div2, "class", "col s12 input-field");
    			add_location(div2, file$4, 41, 4, 1187);
    			attr_dev(div3, "class", "container row");
    			add_location(div3, file$4, 31, 0, 812);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div3, anchor);
    			append_dev(div3, h4);
    			append_dev(div3, t1);
    			append_dev(div3, div0);
    			append_dev(div0, input0);
    			set_input_value(input0, /*amount*/ ctx[0]);
    			append_dev(div0, t2);
    			append_dev(div0, label0);
    			append_dev(div3, t4);
    			append_dev(div3, div1);
    			append_dev(div1, input1);
    			set_input_value(input1, /*note*/ ctx[1]);
    			append_dev(div1, t5);
    			append_dev(div1, label1);
    			append_dev(div3, t7);
    			append_dev(div3, div2);
    			append_dev(div2, a);
    			append_dev(a, i);
    			append_dev(a, t9);

    			dispose = [
    				listen_dev(input0, "input", input0_input_handler),
    				listen_dev(input1, "input", /*input1_input_handler*/ ctx[5]),
    				listen_dev(a, "click", /*submit*/ ctx[2], false, false, false)
    			];
    		},
    		p: function update(ctx, [dirty]) {
    			if (!input0_updating && dirty & /*amount*/ 1) {
    				set_input_value(input0, /*amount*/ ctx[0]);
    			}

    			input0_updating = false;

    			if (dirty & /*note*/ 2 && input1.value !== /*note*/ ctx[1]) {
    				set_input_value(input1, /*note*/ ctx[1]);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div3);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$5.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$3($$self, $$props, $$invalidate) {
    	const expenses = new Table("expenses");
    	let amount, note;

    	const submit = () => {
    		if (note.length == 0 || !note) {
    			M.toast({
    				html: "Please enter a note for the expense",
    				classes: "red white-text"
    			});

    			return;
    		}

    		if (amount < 0) {
    			M.toast({
    				html: `Who would pay you for ${note}?`,
    				classes: "red white-text"
    			});

    			return;
    		}

    		if (!amount) {
    			M.toast({
    				html: "Please enter a valid amount",
    				classes: "red white-text"
    			});

    			return;
    		}

    		expenses.add({ amount, note });

    		M.toast({
    			html: "Expense added successfully",
    			classes: "green darken-1 white-text"
    		});

    		router.setPageByName("home");
    	};

    	function input0_input_handler() {
    		amount = to_number(this.value);
    		$$invalidate(0, amount);
    	}

    	function input1_input_handler() {
    		note = this.value;
    		$$invalidate(1, note);
    	}

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("amount" in $$props) $$invalidate(0, amount = $$props.amount);
    		if ("note" in $$props) $$invalidate(1, note = $$props.note);
    	};

    	return [amount, note, submit, expenses, input0_input_handler, input1_input_handler];
    }

    class Spend extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$3, create_fragment$5, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Spend",
    			options,
    			id: create_fragment$5.name
    		});
    	}
    }

    /* src\components\pages\Tip.svelte generated by Svelte v3.18.1 */
    const file$5 = "src\\components\\pages\\Tip.svelte";

    function get_each_context$1(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[8] = list[i];
    	return child_ctx;
    }

    // (50:20) {#each allJobs as job}
    function create_each_block$1(ctx) {
    	let option;
    	let t_value = /*job*/ ctx[8].name + "";
    	let t;
    	let option_value_value;

    	const block = {
    		c: function create() {
    			option = element("option");
    			t = text(t_value);
    			option.__value = option_value_value = /*job*/ ctx[8].id;
    			option.value = option.__value;
    			add_location(option, file$5, 50, 24, 1461);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, option, anchor);
    			append_dev(option, t);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(option);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$1.name,
    		type: "each",
    		source: "(50:20) {#each allJobs as job}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$6(ctx) {
    	let div5;
    	let h4;
    	let t1;
    	let div4;
    	let div3;
    	let div0;
    	let input;
    	let input_updating = false;
    	let t2;
    	let label0;
    	let t4;
    	let div1;
    	let select;
    	let option;
    	let t6;
    	let label1;
    	let t8;
    	let div2;
    	let a;
    	let i;
    	let t10;
    	let dispose;

    	function input_input_handler() {
    		input_updating = true;
    		/*input_input_handler*/ ctx[6].call(input);
    	}

    	let each_value = /*allJobs*/ ctx[2];
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div5 = element("div");
    			h4 = element("h4");
    			h4.textContent = "Tips";
    			t1 = space();
    			div4 = element("div");
    			div3 = element("div");
    			div0 = element("div");
    			input = element("input");
    			t2 = space();
    			label0 = element("label");
    			label0.textContent = "Tip";
    			t4 = space();
    			div1 = element("div");
    			select = element("select");
    			option = element("option");
    			option.textContent = "Select job";

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t6 = space();
    			label1 = element("label");
    			label1.textContent = "Job";
    			t8 = space();
    			div2 = element("div");
    			a = element("a");
    			i = element("i");
    			i.textContent = "save";
    			t10 = text("Save");
    			add_location(h4, file$5, 39, 4, 975);
    			attr_dev(input, "type", "number");
    			attr_dev(input, "name", "tip");
    			add_location(input, file$5, 43, 16, 1107);
    			attr_dev(label0, "for", "tip");
    			add_location(label0, file$5, 44, 16, 1174);
    			attr_dev(div0, "class", "input-field col s12");
    			add_location(div0, file$5, 42, 12, 1056);
    			option.__value = "";
    			option.value = option.__value;
    			option.disabled = true;
    			option.selected = true;
    			add_location(option, file$5, 48, 20, 1337);
    			if (/*job_id*/ ctx[1] === void 0) add_render_callback(() => /*select_change_handler*/ ctx[7].call(select));
    			add_location(select, file$5, 47, 16, 1287);
    			add_location(label1, file$5, 53, 16, 1579);
    			attr_dev(div1, "class", "input-field col s12");
    			add_location(div1, file$5, 46, 12, 1236);
    			attr_dev(i, "class", "material-icons left");
    			add_location(i, file$5, 56, 100, 1766);
    			attr_dev(a, "href", "#!");
    			attr_dev(a, "class", "waves-effect waves-light btn green darken-2");
    			add_location(a, file$5, 56, 16, 1682);
    			attr_dev(div2, "class", "input-field col s12");
    			add_location(div2, file$5, 55, 12, 1631);
    			attr_dev(div3, "class", "row");
    			add_location(div3, file$5, 41, 8, 1025);
    			attr_dev(div4, "class", "col s12");
    			add_location(div4, file$5, 40, 4, 994);
    			attr_dev(div5, "class", "row wrapper svelte-14aou7x");
    			add_location(div5, file$5, 38, 0, 944);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div5, anchor);
    			append_dev(div5, h4);
    			append_dev(div5, t1);
    			append_dev(div5, div4);
    			append_dev(div4, div3);
    			append_dev(div3, div0);
    			append_dev(div0, input);
    			set_input_value(input, /*tip*/ ctx[0]);
    			append_dev(div0, t2);
    			append_dev(div0, label0);
    			append_dev(div3, t4);
    			append_dev(div3, div1);
    			append_dev(div1, select);
    			append_dev(select, option);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(select, null);
    			}

    			select_option(select, /*job_id*/ ctx[1]);
    			append_dev(div1, t6);
    			append_dev(div1, label1);
    			append_dev(div3, t8);
    			append_dev(div3, div2);
    			append_dev(div2, a);
    			append_dev(a, i);
    			append_dev(a, t10);

    			dispose = [
    				listen_dev(input, "input", input_input_handler),
    				listen_dev(select, "change", /*select_change_handler*/ ctx[7]),
    				listen_dev(a, "click", /*saveTip*/ ctx[3], false, false, false)
    			];
    		},
    		p: function update(ctx, [dirty]) {
    			if (!input_updating && dirty & /*tip*/ 1) {
    				set_input_value(input, /*tip*/ ctx[0]);
    			}

    			input_updating = false;

    			if (dirty & /*allJobs*/ 4) {
    				each_value = /*allJobs*/ ctx[2];
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$1(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$1(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(select, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (dirty & /*job_id*/ 2) {
    				select_option(select, /*job_id*/ ctx[1]);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div5);
    			destroy_each(each_blocks, detaching);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$6.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$4($$self, $$props, $$invalidate) {
    	const tips = new Table("tips");
    	const jobs = new Table("jobs");

    	// Get all jobs
    	const allJobs = jobs.getAll();

    	// Form Data
    	let tip;

    	let job_id;

    	// Submit form event
    	const saveTip = () => {
    		if (!tip) {
    			M.toast({
    				html: "Please enter a tip amount",
    				classes: "red lighten-1 white-text"
    			});

    			return;
    		}

    		if (!job_id) {
    			M.toast({
    				html: "Please select a job from the dropdown",
    				classes: "red lighten-1 white-text"
    			});

    			return;
    		}

    		tips.add({ amount: tip, job_id });

    		M.toast({
    			"html": "$" + tip + " tip added successfully",
    			classes: "green white-text"
    		});

    		router.setPageByName("home");
    	};

    	onMount(() => {
    		var elems = document.querySelectorAll("select");
    		var instances = M.FormSelect.init(elems, {});
    	});

    	function input_input_handler() {
    		tip = to_number(this.value);
    		$$invalidate(0, tip);
    	}

    	function select_change_handler() {
    		job_id = select_value(this);
    		$$invalidate(1, job_id);
    		$$invalidate(2, allJobs);
    	}

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("tip" in $$props) $$invalidate(0, tip = $$props.tip);
    		if ("job_id" in $$props) $$invalidate(1, job_id = $$props.job_id);
    	};

    	return [
    		tip,
    		job_id,
    		allJobs,
    		saveTip,
    		tips,
    		jobs,
    		input_input_handler,
    		select_change_handler
    	];
    }

    class Tip extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$4, create_fragment$6, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Tip",
    			options,
    			id: create_fragment$6.name
    		});
    	}
    }

    /* src\components\pages\Wage.svelte generated by Svelte v3.18.1 */
    const file$6 = "src\\components\\pages\\Wage.svelte";

    function get_each_context$2(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[10] = list[i];
    	return child_ctx;
    }

    // (63:12) {#each jobs.getAll() as job}
    function create_each_block$2(ctx) {
    	let option;
    	let t_value = /*job*/ ctx[10].name + "";
    	let t;
    	let option_value_value;

    	const block = {
    		c: function create() {
    			option = element("option");
    			t = text(t_value);
    			option.__value = option_value_value = /*job*/ ctx[10].id;
    			option.value = option.__value;
    			add_location(option, file$6, 63, 12, 1987);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, option, anchor);
    			append_dev(option, t);
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(option);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$2.name,
    		type: "each",
    		source: "(63:12) {#each jobs.getAll() as job}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$7(ctx) {
    	let div4;
    	let h4;
    	let t1;
    	let div0;
    	let input0;
    	let input0_updating = false;
    	let t2;
    	let label0;
    	let t4;
    	let div1;
    	let input1;
    	let input1_updating = false;
    	let t5;
    	let label1;
    	let t7;
    	let div2;
    	let select;
    	let option;
    	let t9;
    	let label2;
    	let t11;
    	let div3;
    	let a;
    	let i;
    	let t13;
    	let dispose;

    	function input0_input_handler() {
    		input0_updating = true;
    		/*input0_input_handler*/ ctx[7].call(input0);
    	}

    	function input1_input_handler() {
    		input1_updating = true;
    		/*input1_input_handler*/ ctx[8].call(input1);
    	}

    	let each_value = /*jobs*/ ctx[3].getAll();
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$2(get_each_context$2(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div4 = element("div");
    			h4 = element("h4");
    			h4.textContent = "Add Paycheck";
    			t1 = space();
    			div0 = element("div");
    			input0 = element("input");
    			t2 = space();
    			label0 = element("label");
    			label0.textContent = "Amount received";
    			t4 = space();
    			div1 = element("div");
    			input1 = element("input");
    			t5 = space();
    			label1 = element("label");
    			label1.textContent = "Hours worked";
    			t7 = space();
    			div2 = element("div");
    			select = element("select");
    			option = element("option");
    			option.textContent = "Select job";

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t9 = space();
    			label2 = element("label");
    			label2.textContent = "Job";
    			t11 = space();
    			div3 = element("div");
    			a = element("a");
    			i = element("i");
    			i.textContent = "add";
    			t13 = text("Add");
    			add_location(h4, file$6, 50, 4, 1422);
    			attr_dev(input0, "type", "number");
    			attr_dev(input0, "name", "amount");
    			add_location(input0, file$6, 52, 8, 1492);
    			attr_dev(label0, "for", "amount");
    			add_location(label0, file$6, 53, 8, 1557);
    			attr_dev(div0, "class", "col s12 input-field");
    			add_location(div0, file$6, 51, 4, 1449);
    			attr_dev(input1, "type", "number");
    			attr_dev(input1, "name", "hours");
    			add_location(input1, file$6, 56, 8, 1661);
    			attr_dev(label1, "for", "hours");
    			add_location(label1, file$6, 57, 8, 1724);
    			attr_dev(div1, "class", "col s12 input-field");
    			add_location(div1, file$6, 55, 4, 1618);
    			option.__value = "";
    			option.value = option.__value;
    			option.disabled = true;
    			option.selected = true;
    			add_location(option, file$6, 61, 12, 1877);
    			attr_dev(select, "name", "job");
    			if (/*job_id*/ ctx[2] === void 0) add_render_callback(() => /*select_change_handler*/ ctx[9].call(select));
    			add_location(select, file$6, 60, 8, 1824);
    			attr_dev(label2, "for", "job");
    			add_location(label2, file$6, 66, 8, 2081);
    			attr_dev(div2, "class", "col s12 input-field");
    			add_location(div2, file$6, 59, 4, 1781);
    			attr_dev(i, "class", "material-icons left");
    			add_location(i, file$6, 69, 66, 2228);
    			attr_dev(a, "href", "#!");
    			attr_dev(a, "class", "btn green darken-1");
    			add_location(a, file$6, 69, 8, 2170);
    			attr_dev(div3, "class", "col s12 input-field");
    			add_location(div3, file$6, 68, 4, 2127);
    			attr_dev(div4, "class", "container row");
    			add_location(div4, file$6, 49, 0, 1389);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div4, anchor);
    			append_dev(div4, h4);
    			append_dev(div4, t1);
    			append_dev(div4, div0);
    			append_dev(div0, input0);
    			set_input_value(input0, /*amount*/ ctx[0]);
    			append_dev(div0, t2);
    			append_dev(div0, label0);
    			append_dev(div4, t4);
    			append_dev(div4, div1);
    			append_dev(div1, input1);
    			set_input_value(input1, /*hours*/ ctx[1]);
    			append_dev(div1, t5);
    			append_dev(div1, label1);
    			append_dev(div4, t7);
    			append_dev(div4, div2);
    			append_dev(div2, select);
    			append_dev(select, option);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(select, null);
    			}

    			select_option(select, /*job_id*/ ctx[2]);
    			append_dev(div2, t9);
    			append_dev(div2, label2);
    			append_dev(div4, t11);
    			append_dev(div4, div3);
    			append_dev(div3, a);
    			append_dev(a, i);
    			append_dev(a, t13);

    			dispose = [
    				listen_dev(input0, "input", input0_input_handler),
    				listen_dev(input1, "input", input1_input_handler),
    				listen_dev(select, "change", /*select_change_handler*/ ctx[9]),
    				listen_dev(a, "click", /*submit*/ ctx[4], false, false, false)
    			];
    		},
    		p: function update(ctx, [dirty]) {
    			if (!input0_updating && dirty & /*amount*/ 1) {
    				set_input_value(input0, /*amount*/ ctx[0]);
    			}

    			input0_updating = false;

    			if (!input1_updating && dirty & /*hours*/ 2) {
    				set_input_value(input1, /*hours*/ ctx[1]);
    			}

    			input1_updating = false;

    			if (dirty & /*jobs*/ 8) {
    				each_value = /*jobs*/ ctx[3].getAll();
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$2(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$2(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(select, null);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (dirty & /*job_id*/ 4) {
    				select_option(select, /*job_id*/ ctx[2]);
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div4);
    			destroy_each(each_blocks, detaching);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$7.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$5($$self, $$props, $$invalidate) {
    	const jobs = new Table("jobs");
    	const paychecks = new Table("paychecks");

    	onMount(() => {
    		var elems = document.querySelectorAll("select");
    		var instances = M.FormSelect.init(elems, {});
    	});

    	let amount, hours, job_id;

    	const submit = () => {
    		if (!job_id) {
    			M.toast({
    				html: "Please select a job from the dropdown menu",
    				classes: "red white-text"
    			});

    			return;
    		}

    		if (amount < 0) {
    			M.toast({
    				html: "Are YOU paying them???",
    				classes: "red white-text"
    			});

    			return;
    		}

    		if (!amount) {
    			M.toast({
    				html: "Please enter an amount > 0",
    				classes: "red white-text"
    			});

    			return;
    		}

    		if (hours < 0) {
    			M.toast({
    				html: "You cant work negative hours, silly",
    				classes: "red white-text"
    			});

    			return;
    		}

    		if (!hours) {
    			M.toast({
    				html: "Please enter a value > 0 for hours worked",
    				classes: "red white-text"
    			});

    			return;
    		}

    		paychecks.add({ amount, hours, job_id });

    		M.toast({
    			html: "Paycheck added successfully",
    			classes: "green darken-1 white-text"
    		});

    		router.setPageByName("home");
    	};

    	let check = paychecks.getAll()[0];
    	console.log("Paycheck for $" + check.amount, "added on", paychecks.getDateOfCreation(check.id));

    	function input0_input_handler() {
    		amount = to_number(this.value);
    		$$invalidate(0, amount);
    	}

    	function input1_input_handler() {
    		hours = to_number(this.value);
    		$$invalidate(1, hours);
    	}

    	function select_change_handler() {
    		job_id = select_value(this);
    		$$invalidate(2, job_id);
    		$$invalidate(3, jobs);
    	}

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("amount" in $$props) $$invalidate(0, amount = $$props.amount);
    		if ("hours" in $$props) $$invalidate(1, hours = $$props.hours);
    		if ("job_id" in $$props) $$invalidate(2, job_id = $$props.job_id);
    		if ("check" in $$props) check = $$props.check;
    	};

    	return [
    		amount,
    		hours,
    		job_id,
    		jobs,
    		submit,
    		paychecks,
    		check,
    		input0_input_handler,
    		input1_input_handler,
    		select_change_handler
    	];
    }

    class Wage extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$5, create_fragment$7, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Wage",
    			options,
    			id: create_fragment$7.name
    		});
    	}
    }

    /* src\components\pages\settings\Settings.svelte generated by Svelte v3.18.1 */
    const file$7 = "src\\components\\pages\\settings\\Settings.svelte";

    function get_each_context$3(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[10] = list[i];
    	return child_ctx;
    }

    // (34:8) {#each jobs.getAll() as job}
    function create_each_block$3(ctx) {
    	let li;
    	let div;
    	let t0_value = /*job*/ ctx[10].name + "";
    	let t0;
    	let t1;
    	let a;
    	let i;
    	let dispose;

    	function click_handler(...args) {
    		return /*click_handler*/ ctx[7](/*job*/ ctx[10], ...args);
    	}

    	const block = {
    		c: function create() {
    			li = element("li");
    			div = element("div");
    			t0 = text(t0_value);
    			t1 = space();
    			a = element("a");
    			i = element("i");
    			i.textContent = "edit";
    			attr_dev(i, "class", "material-icons");
    			add_location(i, file$7, 38, 24, 1310);
    			attr_dev(a, "href", "#!");
    			attr_dev(a, "class", "secondary-content");
    			add_location(a, file$7, 37, 20, 1157);
    			add_location(div, file$7, 35, 16, 1098);
    			attr_dev(li, "class", "collection-item");
    			add_location(li, file$7, 34, 12, 1052);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, li, anchor);
    			append_dev(li, div);
    			append_dev(div, t0);
    			append_dev(div, t1);
    			append_dev(div, a);
    			append_dev(a, i);
    			dispose = listen_dev(a, "click", click_handler, false, false, false);
    		},
    		p: function update(new_ctx, dirty) {
    			ctx = new_ctx;
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(li);
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block$3.name,
    		type: "each",
    		source: "(34:8) {#each jobs.getAll() as job}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$8(ctx) {
    	let div0;
    	let h3;
    	let t1;
    	let section0;
    	let ul;
    	let li0;
    	let t3;
    	let t4;
    	let li1;
    	let div1;
    	let a0;
    	let i0;
    	let t6;
    	let t7;
    	let section1;
    	let h50;
    	let t9;
    	let p0;
    	let t10;
    	let br;
    	let span1;
    	let span0;
    	let t12;
    	let t13;
    	let a1;
    	let i1;
    	let t15;
    	let t16;
    	let div5;
    	let div3;
    	let h51;
    	let t18;
    	let p1;
    	let t21;
    	let div2;
    	let input;
    	let t22;
    	let label;
    	let t24;
    	let span2;
    	let t25;
    	let t26;
    	let div4;
    	let a2;
    	let t28;
    	let a3;
    	let dispose;
    	let each_value = /*jobs*/ ctx[2].getAll();
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block$3(get_each_context$3(ctx, each_value, i));
    	}

    	const block = {
    		c: function create() {
    			div0 = element("div");
    			h3 = element("h3");
    			h3.textContent = "Settings";
    			t1 = space();
    			section0 = element("section");
    			ul = element("ul");
    			li0 = element("li");
    			li0.textContent = "Jobs";
    			t3 = space();

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t4 = space();
    			li1 = element("li");
    			div1 = element("div");
    			a0 = element("a");
    			i0 = element("i");
    			i0.textContent = "add";
    			t6 = text("Job");
    			t7 = space();
    			section1 = element("section");
    			h50 = element("h5");
    			h50.textContent = "Wipe all data";
    			t9 = space();
    			p0 = element("p");
    			t10 = text("Delete all data from the application. ");
    			br = element("br");
    			span1 = element("span");
    			span0 = element("span");
    			span0.textContent = "Warning:";
    			t12 = text(" It'll be gone forever and cannot be restored");
    			t13 = space();
    			a1 = element("a");
    			i1 = element("i");
    			i1.textContent = "close";
    			t15 = text("\r\n    Clear all data");
    			t16 = space();
    			div5 = element("div");
    			div3 = element("div");
    			h51 = element("h5");
    			h51.textContent = "Please confirm";
    			t18 = space();
    			p1 = element("p");
    			p1.textContent = `To delete all application data, please type in the field below: ${/*wipeConfirmCaptcha*/ ctx[3]}`;
    			t21 = space();
    			div2 = element("div");
    			input = element("input");
    			t22 = space();
    			label = element("label");
    			label.textContent = "Confirm";
    			t24 = space();
    			span2 = element("span");
    			t25 = text(/*dataConfirmError*/ ctx[1]);
    			t26 = space();
    			div4 = element("div");
    			a2 = element("a");
    			a2.textContent = "Close";
    			t28 = space();
    			a3 = element("a");
    			a3.textContent = "Confirm";
    			add_location(h3, file$7, 27, 4, 883);
    			attr_dev(div0, "class", "container");
    			add_location(div0, file$7, 26, 0, 854);
    			attr_dev(li0, "class", "collection-header svelte-1m0vaud");
    			add_location(li0, file$7, 32, 8, 960);
    			attr_dev(i0, "class", "material-icons left");
    			add_location(i0, file$7, 47, 20, 1653);
    			attr_dev(a0, "href", "#!");
    			attr_dev(a0, "class", "btn green darken-2 waves-effect waves-light");
    			add_location(a0, file$7, 46, 16, 1507);
    			add_location(div1, file$7, 45, 12, 1484);
    			attr_dev(li1, "class", "collection-item");
    			add_location(li1, file$7, 44, 8, 1442);
    			attr_dev(ul, "class", "collection");
    			add_location(ul, file$7, 31, 4, 927);
    			add_location(section0, file$7, 30, 0, 912);
    			add_location(h50, file$7, 55, 0, 1807);
    			add_location(br, file$7, 56, 41, 1872);
    			attr_dev(span0, "class", "bold svelte-1m0vaud");
    			add_location(span0, file$7, 56, 67, 1898);
    			attr_dev(span1, "class", "warning svelte-1m0vaud");
    			add_location(span1, file$7, 56, 45, 1876);
    			add_location(p0, file$7, 56, 0, 1831);
    			attr_dev(i1, "class", "material-icons left");
    			add_location(i1, file$7, 58, 4, 2058);
    			attr_dev(a1, "href", "#wipeConfirm");
    			attr_dev(a1, "class", "btn red accent-3 modal-trigger");
    			add_location(a1, file$7, 57, 0, 1990);
    			attr_dev(section1, "class", "container");
    			add_location(section1, file$7, 54, 0, 1778);
    			add_location(h51, file$7, 66, 8, 2242);
    			add_location(p1, file$7, 67, 8, 2275);
    			attr_dev(input, "type", "text");
    			attr_dev(input, "name", "captcha");
    			add_location(input, file$7, 69, 12, 2415);
    			attr_dev(label, "for", "captcha");
    			add_location(label, file$7, 70, 12, 2489);
    			attr_dev(span2, "class", "helper-text red-text");
    			add_location(span2, file$7, 71, 12, 2539);
    			attr_dev(div2, "class", "input-field");
    			add_location(div2, file$7, 68, 8, 2376);
    			attr_dev(div3, "class", "modal-content");
    			add_location(div3, file$7, 65, 4, 2205);
    			attr_dev(a2, "href", "#!");
    			attr_dev(a2, "class", "modal-close btn-flat");
    			add_location(a2, file$7, 75, 8, 2669);
    			attr_dev(a3, "href", "#!");
    			attr_dev(a3, "class", "btn red accent-3");
    			add_location(a3, file$7, 76, 8, 2730);
    			attr_dev(div4, "class", "modal-footer");
    			add_location(div4, file$7, 74, 4, 2633);
    			attr_dev(div5, "class", "modal");
    			attr_dev(div5, "id", "wipeConfirm");
    			add_location(div5, file$7, 64, 0, 2163);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div0, anchor);
    			append_dev(div0, h3);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, section0, anchor);
    			append_dev(section0, ul);
    			append_dev(ul, li0);
    			append_dev(ul, t3);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(ul, null);
    			}

    			append_dev(ul, t4);
    			append_dev(ul, li1);
    			append_dev(li1, div1);
    			append_dev(div1, a0);
    			append_dev(a0, i0);
    			append_dev(a0, t6);
    			insert_dev(target, t7, anchor);
    			insert_dev(target, section1, anchor);
    			append_dev(section1, h50);
    			append_dev(section1, t9);
    			append_dev(section1, p0);
    			append_dev(p0, t10);
    			append_dev(p0, br);
    			append_dev(p0, span1);
    			append_dev(span1, span0);
    			append_dev(span1, t12);
    			append_dev(section1, t13);
    			append_dev(section1, a1);
    			append_dev(a1, i1);
    			append_dev(a1, t15);
    			insert_dev(target, t16, anchor);
    			insert_dev(target, div5, anchor);
    			append_dev(div5, div3);
    			append_dev(div3, h51);
    			append_dev(div3, t18);
    			append_dev(div3, p1);
    			append_dev(div3, t21);
    			append_dev(div3, div2);
    			append_dev(div2, input);
    			set_input_value(input, /*confirmation*/ ctx[0]);
    			append_dev(div2, t22);
    			append_dev(div2, label);
    			append_dev(div2, t24);
    			append_dev(div2, span2);
    			append_dev(span2, t25);
    			append_dev(div5, t26);
    			append_dev(div5, div4);
    			append_dev(div4, a2);
    			append_dev(div4, t28);
    			append_dev(div4, a3);

    			dispose = [
    				listen_dev(a0, "click", /*click_handler_1*/ ctx[8], false, false, false),
    				listen_dev(input, "input", /*input_input_handler*/ ctx[9]),
    				listen_dev(a3, "click", /*wipeData*/ ctx[4], false, false, false)
    			];
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*router, jobs*/ 4) {
    				each_value = /*jobs*/ ctx[2].getAll();
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context$3(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block$3(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(ul, t4);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (dirty & /*confirmation*/ 1 && input.value !== /*confirmation*/ ctx[0]) {
    				set_input_value(input, /*confirmation*/ ctx[0]);
    			}

    			if (dirty & /*dataConfirmError*/ 2) set_data_dev(t25, /*dataConfirmError*/ ctx[1]);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(section0);
    			destroy_each(each_blocks, detaching);
    			if (detaching) detach_dev(t7);
    			if (detaching) detach_dev(section1);
    			if (detaching) detach_dev(t16);
    			if (detaching) detach_dev(div5);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$8.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$6($$self, $$props, $$invalidate) {
    	const jobs = new Table("jobs");
    	const wipeConfirmCaptcha = Math.random().toString(36).substr(2, 9);
    	let confirmation;
    	let dataConfirmError = "";

    	const wipeData = () => {
    		if (confirmation === wipeConfirmCaptcha) {
    			localStorage.clear();
    			M.toast({ html: "Data wiped successfully" });
    			router.setPageByName("settings");
    		} else {
    			$$invalidate(1, dataConfirmError = "Confirmation is not correct. Please enter: " + wipeConfirmCaptcha);
    		}
    	};

    	let modals;
    	let modalInstances;

    	onMount(() => {
    		modals = document.querySelectorAll(".modal");
    		modalInstances = M.Modal.init(modals, {});
    	});

    	const click_handler = job => {
    		router.setPageByName("settings.addJob", { id: job.id, editing: true });
    	};

    	const click_handler_1 = () => {
    		router.setPageByName("settings.addJob");
    	};

    	function input_input_handler() {
    		confirmation = this.value;
    		$$invalidate(0, confirmation);
    	}

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("confirmation" in $$props) $$invalidate(0, confirmation = $$props.confirmation);
    		if ("dataConfirmError" in $$props) $$invalidate(1, dataConfirmError = $$props.dataConfirmError);
    		if ("modals" in $$props) modals = $$props.modals;
    		if ("modalInstances" in $$props) modalInstances = $$props.modalInstances;
    	};

    	return [
    		confirmation,
    		dataConfirmError,
    		jobs,
    		wipeConfirmCaptcha,
    		wipeData,
    		modals,
    		modalInstances,
    		click_handler,
    		click_handler_1,
    		input_input_handler
    	];
    }

    class Settings extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$6, create_fragment$8, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Settings",
    			options,
    			id: create_fragment$8.name
    		});
    	}
    }

    /* src\components\pages\settings\AddJob.svelte generated by Svelte v3.18.1 */
    const file$8 = "src\\components\\pages\\settings\\AddJob.svelte";

    // (85:12) {:else}
    function create_else_block(ctx) {
    	let i;
    	let t1;

    	const block = {
    		c: function create() {
    			i = element("i");
    			i.textContent = "add";
    			t1 = text("\r\n                Add");
    			attr_dev(i, "class", "material-icons left");
    			add_location(i, file$8, 85, 16, 2344);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, i, anchor);
    			insert_dev(target, t1, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(i);
    			if (detaching) detach_dev(t1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_else_block.name,
    		type: "else",
    		source: "(85:12) {:else}",
    		ctx
    	});

    	return block;
    }

    // (82:12) {#if editing}
    function create_if_block_1(ctx) {
    	let i;
    	let t1;

    	const block = {
    		c: function create() {
    			i = element("i");
    			i.textContent = "save";
    			t1 = text("\r\n                Save");
    			attr_dev(i, "class", "material-icons left");
    			add_location(i, file$8, 82, 16, 2244);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, i, anchor);
    			insert_dev(target, t1, anchor);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(i);
    			if (detaching) detach_dev(t1);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(82:12) {#if editing}",
    		ctx
    	});

    	return block;
    }

    // (90:8) {#if editing}
    function create_if_block(ctx) {
    	let a;
    	let i;
    	let t1;

    	const block = {
    		c: function create() {
    			a = element("a");
    			i = element("i");
    			i.textContent = "delete";
    			t1 = text("\r\n                Delete");
    			attr_dev(i, "class", "material-icons left");
    			add_location(i, file$8, 91, 16, 2555);
    			attr_dev(a, "href", "#confirmDelete");
    			attr_dev(a, "class", "btn red darken-1 modal-trigger");
    			add_location(a, file$8, 90, 12, 2473);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, a, anchor);
    			append_dev(a, i);
    			append_dev(a, t1);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(a);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(90:8) {#if editing}",
    		ctx
    	});

    	return block;
    }

    function create_fragment$9(ctx) {
    	let a0;
    	let i0;
    	let t1;
    	let div4;
    	let div0;
    	let input0;
    	let t2;
    	let label0;
    	let t4;
    	let div1;
    	let input1;
    	let input1_updating = false;
    	let t5;
    	let label1;
    	let t7;
    	let div2;
    	let p0;
    	let label2;
    	let input2;
    	let t8;
    	let span;
    	let t10;
    	let div3;
    	let a1;
    	let t11;
    	let t12;
    	let div7;
    	let div5;
    	let h4;
    	let t13;
    	let t14;
    	let t15;
    	let t16;
    	let p1;
    	let t18;
    	let div6;
    	let a2;
    	let t20;
    	let a3;
    	let i1;
    	let t22;
    	let dispose;

    	function input1_input_handler() {
    		input1_updating = true;
    		/*input1_input_handler*/ ctx[12].call(input1);
    	}

    	function select_block_type(ctx, dirty) {
    		if (/*editing*/ ctx[0]) return create_if_block_1;
    		return create_else_block;
    	}

    	let current_block_type = select_block_type(ctx);
    	let if_block0 = current_block_type(ctx);
    	let if_block1 = /*editing*/ ctx[0] && create_if_block(ctx);

    	const block = {
    		c: function create() {
    			a0 = element("a");
    			i0 = element("i");
    			i0.textContent = "arrow_back";
    			t1 = space();
    			div4 = element("div");
    			div0 = element("div");
    			input0 = element("input");
    			t2 = space();
    			label0 = element("label");
    			label0.textContent = "Name";
    			t4 = space();
    			div1 = element("div");
    			input1 = element("input");
    			t5 = space();
    			label1 = element("label");
    			label1.textContent = "Hourly wage";
    			t7 = space();
    			div2 = element("div");
    			p0 = element("p");
    			label2 = element("label");
    			input2 = element("input");
    			t8 = space();
    			span = element("span");
    			span.textContent = "Tips";
    			t10 = space();
    			div3 = element("div");
    			a1 = element("a");
    			if_block0.c();
    			t11 = space();
    			if (if_block1) if_block1.c();
    			t12 = space();
    			div7 = element("div");
    			div5 = element("div");
    			h4 = element("h4");
    			t13 = text("Are you sure you want to delete job \"");
    			t14 = text(/*name*/ ctx[1]);
    			t15 = text("\"?");
    			t16 = space();
    			p1 = element("p");
    			p1.textContent = "It cannot be recovered once it has been deleted.";
    			t18 = space();
    			div6 = element("div");
    			a2 = element("a");
    			a2.textContent = "Cancel";
    			t20 = space();
    			a3 = element("a");
    			i1 = element("i");
    			i1.textContent = "delete";
    			t22 = text("\r\n            Confirm");
    			attr_dev(i0, "class", "material-icons");
    			add_location(i0, file$8, 60, 26, 1372);
    			attr_dev(a0, "href", "#!");
    			attr_dev(a0, "class", "back svelte-v7xjmd");
    			add_location(a0, file$8, 60, 0, 1346);
    			attr_dev(input0, "type", "text");
    			attr_dev(input0, "name", "name");
    			add_location(input0, file$8, 64, 8, 1538);
    			attr_dev(label0, "for", "name");
    			toggle_class(label0, "active", /*editing*/ ctx[0]);
    			add_location(label0, file$8, 65, 8, 1597);
    			attr_dev(div0, "class", "input-field col s12");
    			add_location(div0, file$8, 63, 4, 1495);
    			attr_dev(input1, "type", "number");
    			attr_dev(input1, "name", "wage");
    			add_location(input1, file$8, 68, 8, 1711);
    			attr_dev(label1, "for", "wage");
    			toggle_class(label1, "active", /*editing*/ ctx[0]);
    			add_location(label1, file$8, 69, 8, 1772);
    			attr_dev(div1, "class", "input-field col s12");
    			add_location(div1, file$8, 67, 4, 1668);
    			attr_dev(input2, "type", "checkbox");
    			attr_dev(input2, "name", "tips");
    			add_location(input2, file$8, 74, 16, 1954);
    			add_location(span, file$8, 75, 16, 2027);
    			add_location(label2, file$8, 73, 12, 1929);
    			attr_dev(p0, "class", "checkbox-p svelte-v7xjmd");
    			add_location(p0, file$8, 72, 8, 1893);
    			attr_dev(div2, "class", "input-field col s12");
    			add_location(div2, file$8, 71, 4, 1850);
    			attr_dev(a1, "href", "#!");
    			attr_dev(a1, "class", "btn green darken-2");
    			add_location(a1, file$8, 80, 8, 2141);
    			attr_dev(div3, "class", "input-field col s12");
    			add_location(div3, file$8, 79, 4, 2098);
    			attr_dev(div4, "class", "row");
    			add_location(div4, file$8, 62, 0, 1472);
    			add_location(h4, file$8, 100, 8, 2758);
    			add_location(p1, file$8, 101, 8, 2822);
    			attr_dev(div5, "class", "modal-content");
    			add_location(div5, file$8, 99, 4, 2721);
    			attr_dev(a2, "href", "#!");
    			attr_dev(a2, "class", "modal-close btn-flat");
    			add_location(a2, file$8, 104, 8, 2931);
    			attr_dev(i1, "class", "material-icons left");
    			add_location(i1, file$8, 106, 12, 3078);
    			attr_dev(a3, "href", "#!");
    			attr_dev(a3, "class", "modal-close btn red darken-2");
    			add_location(a3, file$8, 105, 8, 2993);
    			attr_dev(div6, "class", "modal-footer");
    			add_location(div6, file$8, 103, 4, 2895);
    			attr_dev(div7, "class", "modal");
    			attr_dev(div7, "id", "confirmDelete");
    			add_location(div7, file$8, 98, 0, 2677);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, a0, anchor);
    			append_dev(a0, i0);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, div4, anchor);
    			append_dev(div4, div0);
    			append_dev(div0, input0);
    			set_input_value(input0, /*name*/ ctx[1]);
    			append_dev(div0, t2);
    			append_dev(div0, label0);
    			append_dev(div4, t4);
    			append_dev(div4, div1);
    			append_dev(div1, input1);
    			set_input_value(input1, /*wage*/ ctx[2]);
    			append_dev(div1, t5);
    			append_dev(div1, label1);
    			append_dev(div4, t7);
    			append_dev(div4, div2);
    			append_dev(div2, p0);
    			append_dev(p0, label2);
    			append_dev(label2, input2);
    			input2.checked = /*tips*/ ctx[3];
    			append_dev(label2, t8);
    			append_dev(label2, span);
    			append_dev(div4, t10);
    			append_dev(div4, div3);
    			append_dev(div3, a1);
    			if_block0.m(a1, null);
    			append_dev(div3, t11);
    			if (if_block1) if_block1.m(div3, null);
    			insert_dev(target, t12, anchor);
    			insert_dev(target, div7, anchor);
    			append_dev(div7, div5);
    			append_dev(div5, h4);
    			append_dev(h4, t13);
    			append_dev(h4, t14);
    			append_dev(h4, t15);
    			append_dev(div5, t16);
    			append_dev(div5, p1);
    			append_dev(div7, t18);
    			append_dev(div7, div6);
    			append_dev(div6, a2);
    			append_dev(div6, t20);
    			append_dev(div6, a3);
    			append_dev(a3, i1);
    			append_dev(a3, t22);

    			dispose = [
    				listen_dev(i0, "click", /*click_handler*/ ctx[10], false, false, false),
    				listen_dev(input0, "input", /*input0_input_handler*/ ctx[11]),
    				listen_dev(input1, "input", input1_input_handler),
    				listen_dev(input2, "change", /*input2_change_handler*/ ctx[13]),
    				listen_dev(a1, "click", /*submit*/ ctx[4], false, false, false),
    				listen_dev(a3, "click", /*deleteJob*/ ctx[5], false, false, false)
    			];
    		},
    		p: function update(ctx, [dirty]) {
    			if (dirty & /*name*/ 2 && input0.value !== /*name*/ ctx[1]) {
    				set_input_value(input0, /*name*/ ctx[1]);
    			}

    			if (dirty & /*editing*/ 1) {
    				toggle_class(label0, "active", /*editing*/ ctx[0]);
    			}

    			if (!input1_updating && dirty & /*wage*/ 4) {
    				set_input_value(input1, /*wage*/ ctx[2]);
    			}

    			input1_updating = false;

    			if (dirty & /*editing*/ 1) {
    				toggle_class(label1, "active", /*editing*/ ctx[0]);
    			}

    			if (dirty & /*tips*/ 8) {
    				input2.checked = /*tips*/ ctx[3];
    			}

    			if (current_block_type !== (current_block_type = select_block_type(ctx))) {
    				if_block0.d(1);
    				if_block0 = current_block_type(ctx);

    				if (if_block0) {
    					if_block0.c();
    					if_block0.m(a1, null);
    				}
    			}

    			if (/*editing*/ ctx[0]) {
    				if (!if_block1) {
    					if_block1 = create_if_block(ctx);
    					if_block1.c();
    					if_block1.m(div3, null);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}

    			if (dirty & /*name*/ 2) set_data_dev(t14, /*name*/ ctx[1]);
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(a0);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(div4);
    			if_block0.d();
    			if (if_block1) if_block1.d();
    			if (detaching) detach_dev(t12);
    			if (detaching) detach_dev(div7);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$9.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$7($$self, $$props, $$invalidate) {
    	const jobs = new Table("jobs");
    	let { editing = false } = $$props;
    	let { id = null } = $$props;
    	let name;
    	let wage;
    	let tips;

    	if (editing) {
    		let job = jobs.getById(id);
    		name = job.name;
    		wage = job.wage;
    		tips = job.tips;
    	}

    	const submit = () => {
    		if (!name) {
    			M.toast({
    				html: "The name field must not be empty",
    				classes: "red white-text"
    			});

    			return;
    		}

    		if (!wage) {
    			M.toast({
    				html: "The wage field must not be empty",
    				classes: "red white-text"
    			});

    			return;
    		}

    		if (!editing) {
    			jobs.add({ name, wage, tips });

    			M.toast({
    				html: "Job added successfully",
    				classes: "green darken-1 white-text"
    			});
    		} else {
    			jobs.update(id, { name, wage, tips });

    			M.toast({
    				html: "Job saved successfully",
    				classes: "green darken-1 white-text"
    			});
    		}

    		router.setPageByName("settings");
    	};

    	const deleteJob = () => {
    		jobs.delete(id);
    		router.setPageByName("settings");

    		M.toast({
    			html: "Job " + name + " deleted successfully"
    		});
    	};

    	let modals;
    	let modalInstances;

    	onMount(() => {
    		let modals = document.querySelectorAll(".modal");
    		modalInstances = M.Modal.init(modals, {});
    	});

    	const writable_props = ["editing", "id"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<AddJob> was created with unknown prop '${key}'`);
    	});

    	const click_handler = () => {
    		router.setPageByName("settings");
    	};

    	function input0_input_handler() {
    		name = this.value;
    		$$invalidate(1, name);
    	}

    	function input1_input_handler() {
    		wage = to_number(this.value);
    		$$invalidate(2, wage);
    	}

    	function input2_change_handler() {
    		tips = this.checked;
    		$$invalidate(3, tips);
    	}

    	$$self.$set = $$props => {
    		if ("editing" in $$props) $$invalidate(0, editing = $$props.editing);
    		if ("id" in $$props) $$invalidate(6, id = $$props.id);
    	};

    	$$self.$capture_state = () => {
    		return {
    			editing,
    			id,
    			name,
    			wage,
    			tips,
    			modals,
    			modalInstances
    		};
    	};

    	$$self.$inject_state = $$props => {
    		if ("editing" in $$props) $$invalidate(0, editing = $$props.editing);
    		if ("id" in $$props) $$invalidate(6, id = $$props.id);
    		if ("name" in $$props) $$invalidate(1, name = $$props.name);
    		if ("wage" in $$props) $$invalidate(2, wage = $$props.wage);
    		if ("tips" in $$props) $$invalidate(3, tips = $$props.tips);
    		if ("modals" in $$props) modals = $$props.modals;
    		if ("modalInstances" in $$props) modalInstances = $$props.modalInstances;
    	};

    	return [
    		editing,
    		name,
    		wage,
    		tips,
    		submit,
    		deleteJob,
    		id,
    		modalInstances,
    		jobs,
    		modals,
    		click_handler,
    		input0_input_handler,
    		input1_input_handler,
    		input2_change_handler
    	];
    }

    class AddJob extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$7, create_fragment$9, safe_not_equal, { editing: 0, id: 6 });

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "AddJob",
    			options,
    			id: create_fragment$9.name
    		});
    	}

    	get editing() {
    		throw new Error("<AddJob>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set editing(value) {
    		throw new Error("<AddJob>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	get id() {
    		throw new Error("<AddJob>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}

    	set id(value) {
    		throw new Error("<AddJob>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    	}
    }

    // Puts all page components into an object and returns them all at once

    var pages = {
        home: Home,
        edit: Edit,
        spend: Spend,
        tip: Tip,
        wage: Wage,
        settings: Settings,
        "settings.addJob": AddJob,
    };

    /* src\components\Navbar.svelte generated by Svelte v3.18.1 */
    const file$9 = "src\\components\\Navbar.svelte";

    function create_fragment$a(ctx) {
    	let nav;
    	let div0;
    	let a0;
    	let t0;
    	let span0;
    	let t2;
    	let a1;
    	let i0;
    	let t4;
    	let ul0;
    	let li0;
    	let a2;
    	let t6;
    	let li1;
    	let a3;
    	let t8;
    	let ul1;
    	let li2;
    	let div2;
    	let div1;
    	let img;
    	let img_src_value;
    	let t9;
    	let a4;
    	let h4;
    	let t10;
    	let span1;
    	let t12;
    	let li3;
    	let a5;
    	let i1;
    	let t14;
    	let dispose;

    	const block = {
    		c: function create() {
    			nav = element("nav");
    			div0 = element("div");
    			a0 = element("a");
    			t0 = text("Peshka");
    			span0 = element("span");
    			span0.textContent = "WAT";
    			t2 = space();
    			a1 = element("a");
    			i0 = element("i");
    			i0.textContent = "menu";
    			t4 = space();
    			ul0 = element("ul");
    			li0 = element("li");
    			a2 = element("a");
    			a2.textContent = "Settings";
    			t6 = space();
    			li1 = element("li");
    			a3 = element("a");
    			a3.textContent = "InterExchange";
    			t8 = space();
    			ul1 = element("ul");
    			li2 = element("li");
    			div2 = element("div");
    			div1 = element("div");
    			img = element("img");
    			t9 = space();
    			a4 = element("a");
    			h4 = element("h4");
    			t10 = text("Peshka");
    			span1 = element("span");
    			span1.textContent = "WAT";
    			t12 = space();
    			li3 = element("li");
    			a5 = element("a");
    			i1 = element("i");
    			i1.textContent = "settings";
    			t14 = text(" Settings");
    			attr_dev(span0, "id", "wat");
    			attr_dev(span0, "class", "svelte-1dcp8nn");
    			add_location(span0, file$9, 26, 68, 634);
    			attr_dev(a0, "href", "#!");
    			attr_dev(a0, "class", "brand-logo");
    			add_location(a0, file$9, 26, 8, 574);
    			attr_dev(i0, "class", "material-icons");
    			add_location(i0, file$9, 27, 87, 752);
    			attr_dev(a1, "href", "javascript:void(0)");
    			attr_dev(a1, "data-target", "mobile-demo");
    			attr_dev(a1, "class", "sidenav-trigger");
    			add_location(a1, file$9, 27, 8, 673);
    			attr_dev(a2, "href", "#!");
    			add_location(a2, file$9, 29, 16, 857);
    			add_location(li0, file$9, 29, 12, 853);
    			attr_dev(a3, "href", "https://www.interexchange.org/");
    			add_location(a3, file$9, 30, 16, 931);
    			add_location(li1, file$9, 30, 12, 927);
    			attr_dev(ul0, "class", "right hide-on-med-and-down");
    			add_location(ul0, file$9, 28, 8, 800);
    			attr_dev(div0, "class", "nav-wrapper");
    			add_location(div0, file$9, 25, 4, 539);
    			attr_dev(nav, "class", "blue lighten-1");
    			add_location(nav, file$9, 24, 0, 505);
    			if (img.src !== (img_src_value = "/images/sidenav.jpg")) attr_dev(img, "src", img_src_value);
    			attr_dev(img, "alt", "ice cream");
    			add_location(img, file$9, 39, 16, 1169);
    			attr_dev(div1, "class", "background");
    			add_location(div1, file$9, 38, 12, 1127);
    			attr_dev(span1, "id", "wat");
    			attr_dev(span1, "class", "svelte-1dcp8nn");
    			add_location(span1, file$9, 41, 75, 1313);
    			attr_dev(h4, "class", "white-text");
    			add_location(h4, file$9, 41, 46, 1284);
    			attr_dev(a4, "href", "#name");
    			attr_dev(a4, "id", "sidenav-logo");
    			attr_dev(a4, "class", "svelte-1dcp8nn");
    			add_location(a4, file$9, 41, 12, 1250);
    			attr_dev(div2, "class", "user-view");
    			add_location(div2, file$9, 37, 8, 1090);
    			add_location(li2, file$9, 36, 4, 1076);
    			attr_dev(i1, "class", "material-icons");
    			add_location(i1, file$9, 44, 47, 1423);
    			attr_dev(a5, "href", "#!");
    			add_location(a5, file$9, 44, 8, 1384);
    			add_location(li3, file$9, 44, 4, 1380);
    			attr_dev(ul1, "class", "sidenav");
    			attr_dev(ul1, "id", "mobile-demo");
    			add_location(ul1, file$9, 35, 0, 1033);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, nav, anchor);
    			append_dev(nav, div0);
    			append_dev(div0, a0);
    			append_dev(a0, t0);
    			append_dev(a0, span0);
    			append_dev(div0, t2);
    			append_dev(div0, a1);
    			append_dev(a1, i0);
    			append_dev(div0, t4);
    			append_dev(div0, ul0);
    			append_dev(ul0, li0);
    			append_dev(li0, a2);
    			append_dev(ul0, t6);
    			append_dev(ul0, li1);
    			append_dev(li1, a3);
    			insert_dev(target, t8, anchor);
    			insert_dev(target, ul1, anchor);
    			append_dev(ul1, li2);
    			append_dev(li2, div2);
    			append_dev(div2, div1);
    			append_dev(div1, img);
    			append_dev(div2, t9);
    			append_dev(div2, a4);
    			append_dev(a4, h4);
    			append_dev(h4, t10);
    			append_dev(h4, span1);
    			append_dev(ul1, t12);
    			append_dev(ul1, li3);
    			append_dev(li3, a5);
    			append_dev(a5, i1);
    			append_dev(a5, t14);

    			dispose = [
    				listen_dev(a0, "click", /*links*/ ctx[0].home, false, false, false),
    				listen_dev(a2, "click", /*links*/ ctx[0].settings, false, false, false),
    				listen_dev(a5, "click", /*links*/ ctx[0].settings, false, false, false)
    			];
    		},
    		p: noop,
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(nav);
    			if (detaching) detach_dev(t8);
    			if (detaching) detach_dev(ul1);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$a.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$8($$self) {
    	let elems;
    	let instances;

    	onMount(() => {
    		elems = document.querySelectorAll(".sidenav");
    		instances = M.Sidenav.init(elems, {});
    	});

    	const setPage = page => {
    		router.setPageByName(page);
    		instances[0].close();
    	};

    	const links = {
    		home: () => setPage("home"),
    		settings: () => setPage("settings")
    	};

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("elems" in $$props) elems = $$props.elems;
    		if ("instances" in $$props) instances = $$props.instances;
    	};

    	return [links];
    }

    class Navbar extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$8, create_fragment$a, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "Navbar",
    			options,
    			id: create_fragment$a.name
    		});
    	}
    }

    /* src\App.svelte generated by Svelte v3.18.1 */

    const file$a = "src\\App.svelte";

    function create_fragment$b(ctx) {
    	let t0;
    	let div0;
    	let t1;
    	let div1;
    	let a0;
    	let i0;
    	let t3;
    	let ul;
    	let li0;
    	let a1;
    	let t5;
    	let li1;
    	let a2;
    	let i1;
    	let t7;
    	let li2;
    	let a3;
    	let i2;
    	let t9;
    	let li3;
    	let a4;
    	let i3;
    	let current;
    	let dispose;
    	const navbar = new Navbar({ $$inline: true });

    	const block = {
    		c: function create() {
    			create_component(navbar.$$.fragment);
    			t0 = space();
    			div0 = element("div");
    			t1 = space();
    			div1 = element("div");
    			a0 = element("a");
    			i0 = element("i");
    			i0.textContent = "add";
    			t3 = space();
    			ul = element("ul");
    			li0 = element("li");
    			a1 = element("a");
    			a1.textContent = "+Tip";
    			t5 = space();
    			li1 = element("li");
    			a2 = element("a");
    			i1 = element("i");
    			i1.textContent = "add";
    			t7 = space();
    			li2 = element("li");
    			a3 = element("a");
    			i2 = element("i");
    			i2.textContent = "remove";
    			t9 = space();
    			li3 = element("li");
    			a4 = element("a");
    			i3 = element("i");
    			i3.textContent = "create";
    			attr_dev(div0, "class", "wrapper");
    			add_location(div0, file$a, 44, 0, 952);
    			attr_dev(i0, "class", "large material-icons");
    			add_location(i0, file$a, 48, 4, 1133);
    			attr_dev(a0, "href", "javascript:void(0)");
    			attr_dev(a0, "class", "btn-floating btn-large blue lighten-2");
    			add_location(a0, file$a, 47, 2, 1013);
    			attr_dev(a1, "href", "javascript:void(0)");
    			attr_dev(a1, "class", "btn-floating green lighten-1");
    			add_location(a1, file$a, 51, 8, 1198);
    			add_location(li0, file$a, 51, 4, 1194);
    			attr_dev(i1, "class", "material-icons");
    			add_location(i1, file$a, 52, 104, 1412);
    			attr_dev(a2, "href", "javascript:void(0)");
    			attr_dev(a2, "class", "btn-floating green lighten-2");
    			add_location(a2, file$a, 52, 8, 1316);
    			add_location(li1, file$a, 52, 4, 1312);
    			attr_dev(i2, "class", "material-icons");
    			add_location(i2, file$a, 53, 93, 1549);
    			attr_dev(a3, "href", "javascript:void(0)");
    			attr_dev(a3, "class", "btn-floating red");
    			add_location(a3, file$a, 53, 8, 1464);
    			add_location(li2, file$a, 53, 4, 1460);
    			attr_dev(i3, "class", "material-icons");
    			add_location(i3, file$a, 54, 93, 1689);
    			attr_dev(a4, "href", "javascript:void(0)");
    			attr_dev(a4, "class", "btn-floating blue");
    			add_location(a4, file$a, 54, 8, 1604);
    			add_location(li3, file$a, 54, 4, 1600);
    			add_location(ul, file$a, 50, 2, 1184);
    			attr_dev(div1, "class", "fixed-action-btn");
    			add_location(div1, file$a, 46, 0, 979);
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			mount_component(navbar, target, anchor);
    			insert_dev(target, t0, anchor);
    			insert_dev(target, div0, anchor);
    			insert_dev(target, t1, anchor);
    			insert_dev(target, div1, anchor);
    			append_dev(div1, a0);
    			append_dev(a0, i0);
    			append_dev(div1, t3);
    			append_dev(div1, ul);
    			append_dev(ul, li0);
    			append_dev(li0, a1);
    			append_dev(ul, t5);
    			append_dev(ul, li1);
    			append_dev(li1, a2);
    			append_dev(a2, i1);
    			append_dev(ul, t7);
    			append_dev(ul, li2);
    			append_dev(li2, a3);
    			append_dev(a3, i2);
    			append_dev(ul, t9);
    			append_dev(ul, li3);
    			append_dev(li3, a4);
    			append_dev(a4, i3);
    			current = true;

    			dispose = [
    				listen_dev(a0, "click", /*click_handler*/ ctx[2], false, false, false),
    				listen_dev(a1, "click", /*clickEvent*/ ctx[1]("tip"), false, false, false),
    				listen_dev(a2, "click", /*clickEvent*/ ctx[1]("wage"), false, false, false),
    				listen_dev(a3, "click", /*clickEvent*/ ctx[1]("spend"), false, false, false),
    				listen_dev(a4, "click", /*clickEvent*/ ctx[1]("edit"), false, false, false)
    			];
    		},
    		p: noop,
    		i: function intro(local) {
    			if (current) return;
    			transition_in(navbar.$$.fragment, local);
    			current = true;
    		},
    		o: function outro(local) {
    			transition_out(navbar.$$.fragment, local);
    			current = false;
    		},
    		d: function destroy(detaching) {
    			destroy_component(navbar, detaching);
    			if (detaching) detach_dev(t0);
    			if (detaching) detach_dev(div0);
    			if (detaching) detach_dev(t1);
    			if (detaching) detach_dev(div1);
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment$b.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance$9($$self, $$props, $$invalidate) {
    	let instances;

    	window.onpopstate = evt => {
    		if (evt.state !== null) {
    			const oldPage = currentPage;
    			const oldPageParts = oldPage.split(".");

    			if (oldPageParts.length === 1) {
    				router.setPageByName("home");
    				return;
    			}

    			const newPage = oldPageParts.slice(0, oldPageParts.length - 1);
    			router.setPageByName(newPage.join("."));
    		}
    	};

    	onMount(() => {
    		const floatingBtn = document.querySelectorAll(".fixed-action-btn");
    		$$invalidate(0, instances = M.FloatingActionButton.init(floatingBtn, {}));

    		new pages.home({
    				target: document.getElementsByClassName("wrapper")[0]
    			});
    	});

    	const clickEvent = btn => () => {
    		router.setPageByName(btn);
    		instances[0].close();
    	};

    	const click_handler = () => {
    		instances[0].open();
    	};

    	$$self.$capture_state = () => {
    		return {};
    	};

    	$$self.$inject_state = $$props => {
    		if ("instances" in $$props) $$invalidate(0, instances = $$props.instances);
    	};

    	return [instances, clickEvent, click_handler];
    }

    class App extends SvelteComponentDev {
    	constructor(options) {
    		super(options);
    		init(this, options, instance$9, create_fragment$b, safe_not_equal, {});

    		dispatch_dev("SvelteRegisterComponent", {
    			component: this,
    			tagName: "App",
    			options,
    			id: create_fragment$b.name
    		});
    	}
    }

    const app = new App({
    	target: document.body,
    	props: {
    		name: 'world'
    	}
    });

    return app;

}());
//# sourceMappingURL=bundle.js.map
