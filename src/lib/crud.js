const id = () => Date.now() + '_' + Math.random().toString(36).substr(2, 9);

export default class Table {
    constructor(key) {
        this.key = key;
        let json = window.localStorage.getItem(key);

        if(!json) {
            json = "{}"
            window.localStorage.setItem(key, "{}");
        }
        this.data = JSON.parse(json);
    }

    save() {
        window.localStorage.setItem(this.key, JSON.stringify(this.data));
    }

    add(data) {
        let obj = data;
        obj.id = id();

        this.data[obj.id] = obj;
        this.save();

        return obj;
    }

    getById(id) {
        return this.data[id];
    }

    getByValue(key, value) {
        for(let objId in this.data) {
            let obj = this.data[objId];

            for(let field in obj) {
                if(obj[field] === value) return obj;
            }
        }
    }

    getAll() {
        let ret = [];
        for(let objId in this.data) {
            ret.push(this.data[objId]);
        }
        return ret;
    }

    update(id, data) {
        this.data[id] = Object.assign(this.data[id], data);
        this.save();
        return this.data[id];
    }

    delete(id) {
        let deleted = delete this.data[id];
        this.save();

        return deleted;
    }

    getDateOfCreation(id) {
        return new Date(parseInt(id.split("_")[0]));
    }
}

// let jobs = new Table('jobs');
// let paychecks = new Table('paychecks');
// let tips = new Table('tips');
// let expenses = new Table("expenses");

//let job = jobs.add({name: "Dumsers Dairyland", tips: false, wage: 11});

// let job = jobs.getById("1580848227171_orrumjh7q");
// let job = jobs.getByValue("name", "Dumsers Dairyland");
// console.log(job);
// jobs.update(job.id, {wage: 100});
// jobs.delete(job.id);
// console.log(jobs.getAll());

// paychecks.add({amount: 50 * job.wage, hours: 50, job_id: job.id}); 

// let check = paychecks.add({amount: job.wage * 35, hours: 35, job_id: job.id});
// let tip = tips.add({amount: 5, job_id: job.id});

// console.log(tips.update(tip.id, {amount: 10}));

// console.log("paycheck", check.amount);
// console.log("tip", tip.amount, "on job", jobs.getById(tip.job_id).name)