import pages from './pages';

let wrapper;

export const setWrapper = element => wrapper = element;

const setPage = (Page, props, url) => {
    const wrapper = document.getElementsByClassName("wrapper")[0];    
}

const setPageByName = (pageName, props) => {
    currentPage = pageName;
    window.history.pushState({pageName, props}, "PeshkaWAT", "/");
    const Page = pages[pageName];
    const wrapper = document.getElementsByClassName("wrapper")[0];
    wrapper.innerHTML = ""; // Clear any page that is already loaded;
    new Page({
        target: wrapper,
        props
    });
}


export default {
    setPage, setPageByName
}

export let currentPage = "home";