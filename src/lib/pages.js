// Puts all page components into an object and returns them all at once

import Home from '../components/pages/Home.svelte';
import Edit from '../components/pages/Edit.svelte';
import Spend from '../components/pages/Spend.svelte';
import Tip from '../components/pages/Tip.svelte';
import Wage from '../components/pages/Wage.svelte';
import Settings from "../components/pages/settings/Settings.svelte";
import SettingsAddJob from '../components/pages/settings/AddJob.svelte';

export default {
    home: Home,
    edit: Edit,
    spend: Spend,
    tip: Tip,
    wage: Wage,
    settings: Settings,
    "settings.addJob": SettingsAddJob,
}

